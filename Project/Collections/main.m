//
//  main.m
//  Collections
//
//  Created by Alexander Sviridov on 06.03.16.
//  Copyright © 2016 Alexander Sviridov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
