//
//  AppDelegate.h
//  Collections
//
//  Created by Alexander Sviridov on 06.03.16.
//  Copyright © 2016 Alexander Sviridov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

