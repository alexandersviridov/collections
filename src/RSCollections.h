//
//  RSCollections.h
//  POS
//
//  Created by Alexander Sviridov on 11/01/16.
//
//

#import <Foundation/Foundation.h>

@interface RSCollectionPair<__covariant KeyType, __covariant ObjectType> : NSObject

@property KeyType key;
@property ObjectType value;

+ (instancetype)pairWithKey:(KeyType)key value:(ObjectType)value;

@end

@interface NSArray<__covariant ObjectType> (RSCollections)

- (NSArray *)mapArray:(id(^)(ObjectType))mappingBlock;
- (NSDictionary *)mapDictionary:(RSCollectionPair *(^)(ObjectType))mappingBlock;
+ (NSArray *)arrayFromRange:(NSRange)range;
- (NSArray *)flattern;
- (NSArray *)flatternMapArray:(id(^)(ObjectType))mappingBlock;

@end

@interface NSDictionary (RSCollections)

- (NSArray *)mapArray:(id(^)(RSCollectionPair *))mappingBlock;
- (NSArray *)flatternMapArray:(id(^)(RSCollectionPair *))mappingBlock;
- (NSDictionary *)mapDictionary:(RSCollectionPair *(^)(RSCollectionPair *))mappingBlock;
- (NSDictionary *)dictionaryByAddingDictionary:(NSDictionary *)addingDictionary;

@end

@interface NSIndexSet (RSCollections)

- (NSArray *)array;

@end
